package task2.vending;


import task2.vending.drinks.Drink;
import task2.vending.drinks.DrinkImpl;
import task2.vending.drinks.DrinkType;
import task2.vending.drinks.Drinks;

public class Main {

    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Drink[] hotDrinks = new DrinkImpl[]{
                new DrinkImpl(Drinks.Чай, DrinkType.Горячий, 150),
                new DrinkImpl(Drinks.Кофе, DrinkType.Горячий, 160)};
        Drink[] coldDrinks = new DrinkImpl[]{
                new DrinkImpl(Drinks.Кола, DrinkType.Прохладительный, 50),
                new DrinkImpl(Drinks.Спрайт, DrinkType.Прохладительный, 60)};

        VendingMachine vm = new VendingMachine(hotDrinks);
        vm.addMoney(200);

        vm.show();
        vm.giveMeADrink(vm.chooseDrink());

        vm.setDrinks(coldDrinks);
        vm.show();

        vm.giveMeADrink(vm.chooseDrink());
        vm.show();
    }
}