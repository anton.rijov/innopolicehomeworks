package task2.vending;


import task2.vending.drinks.Drink;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VendingMachine {
    private double money = 0;
    private Drink[] drinks;

    public VendingMachine() {
    }

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void addMoney(double money) {
        this.money = money;
    }

    private Drink getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    public void giveMeADrink(int key) {
        if (this.money > 0) {
            Drink drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public void setDrinks(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void show() {
        System.out.println("Вендинговый аппарат:");
        System.out.println("Загружены напитки:" );
        for (int i = 0; i < drinks.length; i++) {
            System.out.println("" + i + ". " + drinks[i].getTitle() + " - " + drinks[i].getPrice());
        }
        System.out.println("Внесено: " + money );
    }

    /**
     * взаимодействие с пользователем
     * @return номер загруженного напитка
     */
    public int chooseDrink(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        boolean valid = false;
        int result = -1;
        while (!valid) {
            System.out.println("Введите номер напитка:");
            try {
                result = Integer.parseInt(bufferedReader.readLine());
                valid = result >= 0 && result < drinks.length;
                if (!valid) {
                    System.out.println("Введенно не то");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
