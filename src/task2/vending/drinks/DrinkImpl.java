package task2.vending.drinks;

/**
 * конструктор, перечисления и взаимодействие с пользователем
 * (хотя использовать ENUM для подобной задачи странно)
 */
public class DrinkImpl implements Drink {

    /**
     * наименование из каталога
     * ENUM
     */
    private Drinks drink;
    private double price;
    /**
     * тип напитка
     * Еще один ENUM
     */
    private DrinkType drinkType;

    /**
     * инициализирующий конструктор
     *
     * @param drink название
     * @param drinkType тип
     * @param price цена
     */
    public DrinkImpl(Drinks drink, DrinkType drinkType, double price) {
        this.drink = drink;
        this.price = price;
        this.drinkType = drinkType;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getTitle() {
        return drink.toString();
    }
}
