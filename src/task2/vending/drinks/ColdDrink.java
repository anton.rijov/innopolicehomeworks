package task2.vending.drinks;

import task2.vending.drinks.Drinks;

public class ColdDrink implements Drink {
    private Drinks drink;
    private double price;

    public ColdDrink(Drinks drink, double price) {
        this.drink = drink;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return drink.toString();
    }
}