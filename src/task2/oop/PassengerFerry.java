package task2.oop;

/**
 * пассажирский паром
 * иллюстрация инкапсуляции и наследования
 */

public class PassengerFerry extends Boat {

    /**
     * пассажирский паром
     *
     * @param speed    скорость
     * @param capacity пассажирская вместимость (чел)
     */
    public PassengerFerry(double speed, int capacity) {
        super(speed, capacity);
    }

    /**
     * тестовый main
     */
    public static void main(String[] args) {
        System.out.println(new PassengerFerry(20, 30));
    }

    @Override
    public String toString() {
        return "пассажирский паром{" +
                "вместимость=" + getCapacity() +
                ", скорость=" + getSpeed() +
                ", максимальная выручка за рейс=" + getIncomeForOneTrip() + '}';
    }

    @Logg
    @Override
    public double getIncomeForOneTrip() {
        return getCapacity() * 20.00;
    }

    @Override
    public void show() {
        System.out.println(this);
    }

    @Logg
    @Override
    public void someTest3ArgsMethod(int i, double d, String s) {
        s = "" + d + i;
        return;
    }

}
