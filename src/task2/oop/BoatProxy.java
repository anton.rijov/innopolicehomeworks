package task2.oop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BoatProxy implements InvocationHandler {

    private final Boatable boat;

    public BoatProxy(Boatable boat) {
        this.boat = boat;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        boolean loggable = false; //определяет наличие аннотации @Logg
        loggable = method.getDeclaredAnnotation(Logg.class) != null; //есть у интерфейса
        Method implementedMethod = null;
        Method[] methods = boat.getClass().getDeclaredMethods();
        System.out.println(methods);
        for (Method method1 : methods) {
            if (method1.getName().equals(method.getName())) {
                System.out.println("implementedMethod " + method1.getName());
                implementedMethod = method1;
                break;
            }
        }
        loggable = loggable || (implementedMethod.getDeclaredAnnotation(Logg.class) != null); // ... или у реализации
        if (loggable) {
            System.out.println("method: " + method.getName());
            List<Class> classList = Arrays.asList(method.getParameterTypes());

            System.out.println(classList);
            List<String> paramTypeNames = classList.stream().map(x -> x.getName()).collect(Collectors.toList());

            System.out.println(paramTypeNames);
            if (args != null) {
                List<Object> argsList = Arrays.asList(args);
                List<String> argTypeNames = Arrays.asList(args).stream().map(x -> x.getClass().getName()).collect(Collectors.toList());
            }

            Class[] paramTypes = method.getParameterTypes();
            if (paramTypes != null) {
                IntStream.range(0, paramTypes.length).mapToObj(i -> "parametr: " +
                        paramTypes[i].getClass().getName()
                        + " name:  " + paramTypes[i].getName()
                        + " value: " + args[i]).forEach(System.out::println);
            } else {
                System.out.println("without any args");
            }
            System.out.println("result type: " + method.getReturnType().toString());
            result = method.invoke(boat, args);
            System.out.println("result: " + result);
            return result;
        } else {
            return method.invoke(boat, args);
        }
    }
}
