package task2.oop;

public interface Boatable {
    public int getCapacity();

    public double getSpeed();

    public double getIncomeForOneTrip();

    @Logg
    public void show();

    void someTest3ArgsMethod(int i, double d, String s);
}
