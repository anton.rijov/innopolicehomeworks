package task2.oop;

/**
 * грузовой паром
 * иллюстрация инкапсуляции и наследования
 */

public class CargoFerry extends Boat {
    private final int cargoCapacity;

    @Override
    public void someTest3ArgsMethod(int i, double d, String s) {

    }

    @Override
    public void show() {
        System.out.println(this);
    }

    /**
     * грузовой паром
     *
     * @param speed         скорость
     * @param capacity      пассажирская вместимость (чел)
     * @param cargoCapacity грузоподемность в крупноразмерных объектах (машинах или контейнерах)
     */
    public CargoFerry(double speed, int capacity, int cargoCapacity) {
        super(speed, capacity);
        this.cargoCapacity = cargoCapacity;
    }

    /**
     * тестовый main
     */
    public static void main(String[] args) {
        System.out.println(new CargoFerry(10, 10, 6));
    }

    /**
     * @return грузоподемность
     */
    public int getCargoCapacity() {
        return cargoCapacity;
    }

    @Override
    public double getIncomeForOneTrip() {
        return getCargoCapacity() * 4000.00 + getCapacity() * 15.00;
    }

    @Override
    public String toString() {
        return "грузовой паром{" +
                "вместимость=" + getCapacity() +
                ", грузоподъемность=" + getCapacity() +
                ", скорость=" + getSpeed() +
                ", максимальная выручка за рейс=" + getIncomeForOneTrip() + '}';
    }
}
