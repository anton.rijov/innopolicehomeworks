package task2.oop;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * переправа
 * иллюстрация инкапсуляции и полиморфизма
 * а заодно и коллекций
 */
public class FerryCrossing {

    /**
     * ширина переправы
     */
    private final double distance;

    /**
     * список кораблей
     */
    private final Map<String, Boat> boats = new HashMap<>();

    /**
     * инициализирующий конструктор
     *
     * @param distance ширина
     */
    public FerryCrossing(double distance) {
        this.distance = distance;
    }

    /**
     * тестовый main c перпеправой из двух катеров и автомобильного парома
     */
    public static void main(String[] args) {
        FerryCrossing ferryCrossing = new FerryCrossing(10);
        ferryCrossing.addBoat("катер \"Бочёнок\"", new PassengerFerry(20, 30));
        ferryCrossing.addBoat("катер \"Быстрый\"", new PassengerFerry(22, 35));
        ferryCrossing.addBoat("автомобильный паром \"Толстяк\"", new CargoFerry(10, 15, 10));
        System.out.println(ferryCrossing);

        Boatable aBoat = new PassengerFerry(22, 35);
        Boatable bBoat = new PassengerFerry(5, 5);

        Boatable bBoatProxy = (Boatable) Proxy.newProxyInstance(
                BoatProxy.class.getClassLoader(),
                new Class[]{Boatable.class},
                new BoatProxy(bBoat));

        aBoat.show();
        bBoat.show();
        bBoatProxy.show();

        System.out.println(bBoatProxy.getIncomeForOneTrip());
        bBoatProxy.someTest3ArgsMethod(1, 2, "3");
    }

    /**
     * добавить транспортное средство
     *
     * @param boatName имя
     * @param boat     плавучее транспортное средство
     */
    public void addBoat(String boatName, Boat boat) {
        boats.put(boatName, boat);
    }

    /**
     * максимальная выручка со всех кораблей переправы за сутки
     */
    public double getIncomeForOneDay() {
        double result = 0.00;
        for (Map.Entry<String, Boat> entry : boats.entrySet()) {
            Boat boat = entry.getValue();
            result += boat.getIncomeForOneTrip() * 24 * boat.getSpeed() / distance;
        }
        return result;
    }

    @Override
    public String toString() {
        return "переправа{" +
                "ширина=" + distance +
                ", корабли=" + boats +
                ", максимальная выручка за сутки=" + getIncomeForOneDay() +
                '}';
    }
}
