package task2.oop;

/**
 * абстрактный класс плавучих транспортных средств для организации переправ
 * иллюстрация инкапсуляции и полиморфизма
 */

public abstract class Boat implements Boatable {
    /**
     * скорость
     */
    private final double speed;

    /**
     * пассажирская вместимость (чел)
     */
    private final int capacity;

    /**
     * инициализирующий конструктор
     *
     * @param speed    скорость
     * @param capacity пассажирская вместимость (чел)
     */
    public Boat(double speed, int capacity) {
        this.speed = speed;
        this.capacity = capacity;
    }

    /**
     * @return пассажирскую вместимость (чел)
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @return скорость
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * @return выручка за один рейс
     */

    public abstract double getIncomeForOneTrip();

}
