package task3;

/**
 * Заставить программу ”Hello, World!” упасть
 * ● Смоделировав ошибку «NullPointerException»
 * ● Смоделировав ошибку «ArrayIndexOutOfBoundsException»
 * ● Вызвав свой вариант ошибки через оператор throw
 *
 * под ”Hello, World!” подразумевается простейшая программа с логикой в main
 * если раскомментировать строки обработки исключений программа не будет падать, но она должна падать по условию
 */

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hallo World!");

//        try {
            Object obj = null;
            System.out.println(obj.equals("")); //NullPointerException
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        try {
            int[] array = new int[2];
            System.out.println(array[22]); //ArrayIndexOutOfBoundsException
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        try {
            throw new UnsupportedOperationException("this operation forbidden"); //CustomException
//        } catch (UnsupportedOperationException e) {
//            e.printStackTrace();
//        }
    }
}
