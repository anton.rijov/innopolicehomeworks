package task10;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FactorialCalculatonThreadPool {

    private List<Future<BigInteger>> results = new ArrayList<>();
    private List<BigInteger> arguments = new ArrayList<>();
    private ExecutorService threadPool;
    private CountDownLatch startSignal, doneSignal;

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        FactorialCalculatonThreadPool calculatorPool = new FactorialCalculatonThreadPool();
        calculatorPool.initArithmeticSequenceData(1_000, 40);
        calculatorPool.startMultiThreadCalculations(4);
        calculatorPool.waitAllThreads();
        calculatorPool.printResults();
    }

    /**
     * заполняем коллекцию аргументов арифметической последовательностью
     *
     * @param startNumber         начальный элемент
     * @param lengthOfTheSequence длина последовательности
     */
    public void initArithmeticSequenceData(int startNumber, int lengthOfTheSequence) {
        for (int i = 0; i < lengthOfTheSequence; i++) {
            arguments.add(i, BigInteger.valueOf(startNumber + i));
        }
    }

    /**
     * запускаем вычисления
     *
     * @param threadPoolSize количество потоков
     * @return успешность запуска
     */
    public boolean startMultiThreadCalculations(int threadPoolSize) {
        try {
            startSignal = new CountDownLatch(1);
            doneSignal = new CountDownLatch(arguments.size());
            threadPool = Executors.newFixedThreadPool(threadPoolSize);

            int bound = arguments.size();
            for (int i = 0; i < bound; i++) {
                results.add(i,
                        threadPool.submit(
                                new CallableFactorialCalculator(startSignal, doneSignal, arguments.get(i))));
            }

            System.out.println("CallableFactorialCalculator " + arguments.size() + " threads submit");
            threadPool.shutdown(); //no more tasks
            startSignal.countDown(); //погнали!
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * ждать завершения всех вычислений
     */
    public void waitAllThreads() {
        try {
            doneSignal.await();
            System.out.println("CallableFactorialCalculator " + arguments.size() + " threads finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * распечатать результаты вычислений в два столбца
     */
    public void printResults() {
        try {
            if (!threadPool.isTerminated()) {
                throw new Exception("Calculation not finished yet");
            }
            for (int i = 0; i < arguments.size(); i++) {
                System.out.println("Argument = " + arguments.get(i) + ", Result = " + results.get(i).get());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
