package task10;

import java.math.BigInteger;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class CallableFactorialCalculator implements Callable<BigInteger> {

    private static final Logger log = Logger.getLogger(CallableFactorialCalculator.class.getSimpleName());

    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;
    private BigInteger argument;

    /**
     * поточный вычислитель факториала
     *
     * @param startSignal отсчет "на старт!"
     * @param doneSignal  отсчет "расчет закончил!"
     * @param argument    аргумент функции
     */
    public CallableFactorialCalculator(CountDownLatch startSignal, CountDownLatch doneSignal, BigInteger argument) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
        this.argument = argument;
    }

    /**
     * вычисление фактолриала числа распараллеленым или обычным стримом
     *
     * @param number аргумент функции
     * @return фактолриал
     */
    public static BigInteger factorial(BigInteger number) {
        if (number.intValue() < 2) {
            return BigInteger.ONE;
        }
        boolean singleThreadWillBeFaster = (number.intValue() < 100);
        if (singleThreadWillBeFaster) {
            return IntStream.rangeClosed(2, number.intValue())
                    .mapToObj(BigInteger::valueOf)
                    .reduce(BigInteger::multiply)
                    .get();
        } else {
            return IntStream.rangeClosed(2, number.intValue())
                    .parallel()
                    .mapToObj(BigInteger::valueOf)
                    .reduce(BigInteger::multiply)
                    .get();
        }

    }

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        long start = System.currentTimeMillis();
        BigInteger argument = BigInteger.valueOf(99_999);
        log.info(String.valueOf(factorial(argument)));
        log.info("Argument = " + argument + "\nDuration = " + (System.currentTimeMillis() - start) / 1000);
    }

    @Override
    public BigInteger call() throws Exception {

        startSignal.await();

        Thread.currentThread().setName(getClass().getSimpleName() + " ThreadId = " + Thread.currentThread().getId());
        log.info("Started. Thread = " + Thread.currentThread().getId() + ",  Argument = " + argument);

        BigInteger result = factorial(argument);

        log.info("Finished. Thread = " + Thread.currentThread().getId() + ",  Argument = " + argument + ", Result = " + result);

        doneSignal.countDown();
        return result;
    }
}

