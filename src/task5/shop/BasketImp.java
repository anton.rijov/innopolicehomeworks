package task5.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * корзина покупоек в интернет магазине
 * реализация имнтерфейса Basket
 */

public class BasketImp implements Basket {

    private final Map<String, Integer> basket = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        if (basket.containsKey(product)) {
            basket.put(product, basket.get(product) + quantity);
        } else {
            basket.put(product, quantity);
        }
    }

    @Override
    public void removeProduct(String product) {
        if (basket.containsKey(product)) {
            basket.remove(product);
        } else {
            throw new UnsupportedOperationException(product + " отсутствует в корзине");
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        basket.put(product, quantity);
    }

    @Override
    public void clear() {
        basket.clear();
    }

    @Override
    public List<String> getProducts() {
        return new ArrayList<>(basket.keySet());
    }

    @Override
    public int getProductQuantity(String product) {
        return basket.getOrDefault(product, 0);
    }

    @Override
    public String toString() {
        return basket.toString();
    }

    /**
     * тестируем реализацию интерфейса
     */
    public static void main(String[] args) {
        Basket basketImp = new BasketImp();
        basketImp.addProduct("шапка", 1);
        basketImp.addProduct("варежка", 1);
        basketImp.addProduct("варежка", 1);
        basketImp.addProduct("валенок", 1);
        System.out.println("корзина: " + basketImp);

        System.out.println("список товаров: " + basketImp.getProducts());

        basketImp.removeProduct("валенок");
        System.out.println("корзина без валенка: " + basketImp);

        basketImp.clear();
        System.out.println("очищенная корзина: " + basketImp);

        basketImp.updateProductQuantity("шарф", 5);


        System.out.println("шарфов принудительно 5: " + basketImp);

        basketImp.updateProductQuantity("шарф", 7);
        System.out.println("шарфов принудительно 7: " + basketImp);
    }
}
