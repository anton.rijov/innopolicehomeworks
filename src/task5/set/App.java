package lesson_07.set;

import lesson_07.utils.Person;
import lesson_07.utils.PersonComparator;
import lesson_07.utils.PersonSimple;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by arty on 12.09.2018.
 */
public class App {
    public static void main(String[] args) {

       Set<PersonSimple> peopleSimple = new TreeSet<>(new PersonComparator());
        peopleSimple.add(new PersonSimple("Tom"));
        peopleSimple.add(new PersonSimple("Nick"));
        peopleSimple.add(new PersonSimple("Alice"));
        peopleSimple.add(new PersonSimple("Bill"));

        Set<Person> people = new TreeSet<>();
        people.add(new Person("Tom"));
        people.add(new Person("Nick"));
        people.add(new Person("Alice"));
        people.add(new Person("Bill"));

        iterate(peopleSimple);
        printSet(peopleSimple);

        iterate(people);
        printSet(people);
    }

    private static <T> void  printSet(Set<T> people) {
        System.out.println("Start printing");
        for (T person : people) {
            System.out.println(person);
        }
        System.out.println("Finish printing");
        System.out.println();
    }

    private static <T> void iterate(Set<T> people) {
        Iterator i = people.iterator();
        System.out.println("Start iterating");
        while (i.hasNext()) {
            System.out.print(i.next());
            System.out.print(" : ");
        }
        System.out.println();
        System.out.println("Finish iterating");
        System.out.println();
    }
}
