package lesson_07.utils;

/**
 * Created by arty on 12.09.2018.
 */
public class PersonSimple {

    private String name;

    public PersonSimple(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PersonSimple{" +
                "name='" + name + '\'' +
                '}';
    }
}
