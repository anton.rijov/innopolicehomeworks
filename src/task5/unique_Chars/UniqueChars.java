package lesson_07.unique_Chars;


import java.util.HashMap;
import java.util.Map;

public class UniqueChars {

    private final Map<Character, Integer> characterIntegerMap = new HashMap<>();
    private String text;

    /**
     * офрмление результата
     * @return текст отчета
     */
    public String getText() {
        StringBuilder sb = new StringBuilder("Частотный анализ текста:\n");
        for (Map.Entry<Character, Integer> entry : characterIntegerMap.entrySet()) {
            sb.append("символ \'").append(entry.getKey()).append("\' встречается ").append(entry.getValue()).append(" раз");
            if ((entry.getValue() % 10) >= 2 && (entry.getValue() % 10) <= 4) { //число в родительном падеже
                sb.append('а');
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * установка входных данных для анализа
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * производит частотный анализ
     * результат анализа доступен через метод getText()
     */
    public void calculate() {
        characterIntegerMap.clear();
        for (char c : text.toCharArray()) {
            if (characterIntegerMap.containsKey(c)) {
                characterIntegerMap.put(c, characterIntegerMap.get(c) + 1);
            } else {
                characterIntegerMap.put(c, 1);
            }
        }
    }
}
