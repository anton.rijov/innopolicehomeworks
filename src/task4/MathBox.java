package task4;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class MathBox {
    private Set<Number> numbers;

    /**
     * @param numbers инициализация
     */
    public MathBox(Number... numbers) {
        this.numbers = new HashSet(Arrays.asList(numbers));
    }

    /**
     * тестовый main с иллюстрацией различных хранимых типов
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(new MathBox(1, 2, 3));
        System.out.println(new MathBox(1, 2, 3).sumator());
        System.out.println(new MathBox(new BigDecimal(5), new BigDecimal("100000000000000000000")).sumator());
        System.out.println(new MathBox(1.1, 2.2, 3.7).sumator());
        System.out.println(new MathBox(9.9, 6.6, 3.3).splitter(3.0));
    }

    /**
     * сумматор
     * реализация через for i
     */
    public Number sumator() {
        if (numbers == null || numbers.isEmpty()) {
            return null;
        }
        ArrayList<Number> arrayList = new ArrayList<>(numbers);
        Number sum = arrayList.get(0);
        for (int i = 1; i < arrayList.size(); i++) {
            if (sum == null) {
                return null;
            } else if (sum instanceof BigDecimal) {
                sum = ((BigDecimal) sum).add((BigDecimal) arrayList.get(i));
            } else if (sum instanceof BigInteger) {
                sum = ((BigInteger) sum).add((BigInteger) arrayList.get(i));
            } else if (sum instanceof Byte) {
                sum = (Byte) sum + (Byte) arrayList.get(i);
            } else if (sum instanceof Double) {
                sum = (Double) sum + (Double) arrayList.get(i);
            } else if (sum instanceof Float) {
                sum = (Float) sum + (Float) arrayList.get(i);
            } else if (sum instanceof Integer) {
                sum = (Integer) sum + (Integer) arrayList.get(i);
            } else if (sum instanceof Long) {
                sum = (Long) sum + (Long) arrayList.get(i);
            } else if (sum instanceof Short) {
                sum = (Short) sum + (Short) arrayList.get(i);
            } else {
                throw new UnsupportedOperationException("Not support any but subclasses of Number");
            }
        }
        return sum;
    }

    @Override
    public String toString() {
        return "ru.rijov.task4.MathBox{" +
                "numbers=" + numbers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MathBox)) return false;
        MathBox mathBox = (MathBox) o;
        return Objects.equals(numbers, mathBox.numbers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbers);
    }


    /**
     * сплиттер
     * реализация через foreach
     *
     * @param divider делитель
     * @return заполненный частными MathBox
     */
    public MathBox splitter(Number divider) {
        if (divider.doubleValue() == 0) {
            throw new ArithmeticException("cant split with zero");
        }
        if (numbers == null || numbers.isEmpty()) {
            throw new UnsupportedOperationException("cant operate with empty Set");
        }
        Set<Number> result = new HashSet();
        for (Number number : numbers) {
            if (number instanceof BigDecimal) {
                result.add(((BigDecimal) number).divide((BigDecimal) divider));
            } else if (number instanceof BigInteger) {
                result.add(((BigInteger) number).divide((BigInteger) divider));
            } else if (number instanceof Byte) {
                result.add(((Byte) number / (Byte) divider));
            } else if (number instanceof Double) {
                result.add(((Double) number) / ((Double) divider));
            } else if (number instanceof Float) {
                result.add(((Float) number) / ((Float) divider));
            } else if (number instanceof Integer) {
                result.add(((Integer) number) / ((Integer) divider));
            } else if (number instanceof Long) {
                result.add(((Long) number) / ((Long) divider));
            } else if (number instanceof Short) {
                result.add(((Short) number) / ((Short) divider));
            } else {
                throw new UnsupportedOperationException("Not support any but subclasses of Number");
            }
        }
        numbers = result;
        return this;
    }
}

