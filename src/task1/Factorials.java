package task1;

import java.math.*;

public class Factorials {
    public static void main(String[] args) {
//      alt+shift+f10 and enter int numbers. example 2 10 15 20
        for (String arg : args) {
            System.out.println(factorial(BigInteger.valueOf(Integer.parseInt(arg))));
        }
    }

    private static BigInteger factorial(BigInteger number) {
        BigInteger result = BigInteger.valueOf(1);

        for (long factor = 2; factor <= number.longValue(); factor++) {
            result = result.multiply(BigInteger.valueOf(factor));
        }

        return result;
    }
}
