package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GasolineCostCalculator {
    private double price;

    public GasolineCostCalculator(double price) {
        this.price = price;
    }

    public static void main(String[] args) {
        GasolineCostCalculator calculator = new GasolineCostCalculator(50.00);
        calculator.printTotalText(calculator.askAmount());
    }

    private void printTotalText(double amount) {
        System.out.println("total = " + amount * price);
    }

    private double askAmount() {
        try {
            System.out.print("gasoline amount:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return Double.parseDouble(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0.00;
    }
}
