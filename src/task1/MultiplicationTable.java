package task1;

public class MultiplicationTable {
    private int upperBound;

    public MultiplicationTable(int upperBound) {
        this.upperBound = upperBound;
    }

    public static void main(String[] args) {
        //alt+shift+f10 and enter int number. example 8
        MultiplicationTable table = new MultiplicationTable(Integer.parseInt(args[0]));
        table.show();
    }

    private void show() {
        System.out.println("multiplication table up to " + upperBound);
        System.out.print("    |");
        for (int i = 1; i <= upperBound; i++) {
            System.out.print(String.format(" %3d", i));
        }
        System.out.printf("\n%s%n", new String(new char[(upperBound + 1) * 4 + 1]).replace('\0', '-'));
        for (int row = 1; row <= upperBound; row++) {
            System.out.print(String.format("%3d |", row));
            for (int col = 1; col <= upperBound; col++) {
                System.out.print(String.format(" %3d", row * col));
            }
            System.out.println();
        }
    }

}
