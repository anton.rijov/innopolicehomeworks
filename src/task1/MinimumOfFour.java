package task1;

public class MinimumOfFour {
    public static void main(String[] args) {
//        alt+shift+f10 and enter four int numbers. example 10 20 5 600
        int min = Integer.parseInt(args[0]);
        for (int i = 1; i < 4; i++) {
            int value = Integer.parseInt(args[i]);
            if ( value < min) {
                min = value;
            }
        }
        System.out.println("minimum of four is " + min);
    }
}
