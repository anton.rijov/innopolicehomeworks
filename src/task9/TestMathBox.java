package task9;

import java.lang.reflect.Proxy;

/**
 * тест
 * отдельно логика MathBox
 * отдельно тест проксирования и рефлексия
 */

public class TestMathBox {
    public static void main(String[] args) {
        simpleLogicTest();
        proxyTest();
    }

    public static void simpleLogicTest() {
        System.out.println("\nTest\n");
//        MathBox mathBox = new MathBox();
//        mathBox.add(10);
//        mathBox.add(20);
//        mathBox.add(0);
//        mathBox.add(-1000);
        MathBox mathBox = new MathBox(10, 20, 0, -1000);
        System.out.println("MathBox :" + mathBox);
        System.out.println("Get x 2: " + mathBox.getMultiplied(2));
        System.out.println("Min: " + mathBox.getMin());
        System.out.println("Max: " + mathBox.getMax());
        mathBox.remove(0);
        System.out.println("Remove zero: " + mathBox);
        System.out.println("Sum: " + mathBox.getTotal());
        System.out.println("Avg: " + mathBox.getAverage());
        System.out.println("Map: " + mathBox.getMap());
    }

    public static void proxyTest() {
        System.out.println("\nProxy Test\n");
        MathBoxable aMathBox = (MathBoxable) Proxy.newProxyInstance(
                MathBox.class.getClassLoader(),
                new Class[]{MathBoxable.class},
                new MathBoxProxy(new MathBox(1, 2, 3, 4, 5, 6)));
        System.out.println(aMathBox);
        aMathBox.add(7);
        aMathBox.remove(7);
        System.out.println(aMathBox);
    }

}
