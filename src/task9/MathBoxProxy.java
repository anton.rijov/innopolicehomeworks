package task9;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * проксирующий объект, анализирующий аннотации методов и реагирующий на них
 */

public class MathBoxProxy implements InvocationHandler {

    private MathBox proxiedMathBox;

    public MathBoxProxy(MathBox proxiedMathBox) {
        this.proxiedMathBox = proxiedMathBox;
    }

    boolean isClearDataAnnotated(Method method) {
        return method.isAnnotationPresent(ClearData.class);
    }

    boolean isLogDataAnnotated(Method method) {
        return method.isAnnotationPresent(LogData.class);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        Object result;
        if (isLogDataAnnotated(method)) {
            System.out.println("elements before: " + proxiedMathBox.getElements());
        }

        result = method.invoke(proxiedMathBox, args);

        if (isClearDataAnnotated(method)) {
            proxiedMathBox.getElements().clear();
            System.out.println("elements cleared!");
        }
        if (isLogDataAnnotated(method)) {
            System.out.println("elements after: " + proxiedMathBox.getElements());
        }
        return result;
    }
}
