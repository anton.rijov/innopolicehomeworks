package task9;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


public class MathBox implements MathBoxable {

    private Set<Integer> elements;

    /**
     * создает MathBox и инициализирует коллекцию массивом параметров
     *
     * @param args элементы для добавления в коллекцию
     */
    public MathBox(int... args) {
        this();
        for (int i : args) {
            elements.add(i);
        }
    }

    /**
     * создает MathBox c пустой коллекцией
     */
    public MathBox() {
        this.elements = new HashSet<>();
    }

    public Set<Integer> getElements() {
        return elements;
    }

    @Override
    public boolean remove(int i) {
        return elements.remove(i);
    }

    @Override
    public boolean add(int i) {
        return elements.add(i);
    }

    @Override
    public int getTotal() {
        int total = 0;
        for (Integer i : elements) {
            total += i;
        }
        return total;
    }

    @Override
    public int getAverage() {
        if (elements.size() == 0) {
            return 0;
        } else {
            return Math.round(getTotal() / elements.size());
        }
    }

    @Override
    public Set<Integer> getMultiplied(int multiplicator) {
        //коллекцию в поток, поэлементно умножаем, собираем из потока новую коллекцию
        Set<Integer> newCollection = elements.stream().map(integer -> integer * multiplicator).collect(Collectors.toSet());
        elements = newCollection; //заменяем старую коллекцию новой
        return elements;
    }

    @Override
    public Map<Integer, String> getMap() {
        //коллекцию в поток, собираем из потока новую коллекцию
        return elements.stream().collect(Collectors.toMap(k -> k, v -> v.toString()));
    }

    @Override
    public int getMax() {
        return elements.stream().max(Integer::compare).get();
//        или через цикл по-старинке
//        int max = Integer.MIN_VALUE;
//        for (Integer element : elements) {
//            if (element > max) {
//                max = element;
//            }
//        }
//        return max;
    }

    @Override
    public int getMin() {
        return elements.stream().min(Integer::compare).get();
    }

    @Override
    public String toString() {
        return "MathBox: { elements: " + elements.toString() + " }";
    }
}
